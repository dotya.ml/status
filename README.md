# [status](https://git.dotya.ml/dotya.ml/status)

this repo holds configs of [dotya.ml's Statping-ng instance](https://status.dotya.ml/).

### LICENSE
WTFPLv2, see [LICENSE](LICENSE) for details
